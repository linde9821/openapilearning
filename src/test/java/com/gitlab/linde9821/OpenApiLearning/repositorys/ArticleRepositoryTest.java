package com.gitlab.linde9821.OpenApiLearning.repositorys;

import com.gitlab.linde9821.OpenApiLearning.mod.InternalArticle;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ArticleRepositoryTest {
    @Autowired
    private ArticleRepository articleRepository;

    @Test
    void saveArticles_given1RandomArticles_CorrectlySaved() {
        InternalArticle randomArticle = InternalArticle.builder()
                .date(LocalDate.now())
                .description("TEst")
                .imageUrl("ddd")
                .title("djdd")
                .id(UUID.randomUUID())
                .build();

        InternalArticle savedArticle = articleRepository.save(randomArticle);

        assertEquals(randomArticle, savedArticle);
    }
}