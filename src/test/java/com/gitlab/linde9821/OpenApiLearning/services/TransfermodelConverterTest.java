package com.gitlab.linde9821.OpenApiLearning.services;

import com.gitlab.linde9821.OpenAPILearning.generated.transfermodel.Article;
import com.gitlab.linde9821.OpenApiLearning.mod.InternalArticle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

class TransfermodelConverterTest {

    private TransfermodelConverter transfermodelConverter;

    @BeforeEach
    void setUp() {
        transfermodelConverter = new TransfermodelConverter();
    }

    @Test
    void transferToInternal() {
        final UUID expectedId = UUID.randomUUID();

        com.gitlab.linde9821.OpenAPILearning.generated.transfermodel.Article articleToConvert =
                new com.gitlab.linde9821.OpenAPILearning.generated.transfermodel.Article();
        articleToConvert.setDate(LocalDate.EPOCH);
        articleToConvert.setDescription("");
        articleToConvert.setImageUrl("");
        articleToConvert.setTitle("");

        InternalArticle expectedResult = InternalArticle.builder()
                .date(LocalDate.EPOCH)
                .id(expectedId)
                .description("")
                .imageUrl("")
                .title("")
                .build();

        Assertions.assertEquals(expectedResult,
                transfermodelConverter.TArticleToMArticle(articleToConvert));
    }

    @Test
    void internalToTransfer() {
        final UUID expectedId = UUID.randomUUID();

        InternalArticle internalArticleToConvert = InternalArticle.builder()
                .date(LocalDate.EPOCH)
                .id(expectedId)
                .description("")
                .imageUrl("")
                .title("")
                .build();

        Article expectedResult = new Article();
        expectedResult.setId(expectedId);
        expectedResult.setDate(LocalDate.EPOCH);
        expectedResult.setDescription("");
        expectedResult.setImageUrl("");
        expectedResult.setTitle("");

        Assertions.assertEquals(expectedResult,
                transfermodelConverter.MArticleToTArticle(internalArticleToConvert));
    }

}