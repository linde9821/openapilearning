package com.gitlab.linde9821.OpenApiLearning.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class InternalArticleServiceTest {
    private ArticleService articleService;

    @BeforeEach
    void setUp() {
        articleService = new ArticleService();
    }

    @Test
    void buildArticle_GivenValidInput_CorrectlyBuild() {
        assertNotNull(articleService.getRandomArticle());
    }

}