package com.gitlab.linde9821.OpenApiLearning.mod;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InternalArticleTest {
    /**
     * Test person builder.
     */
    @Test
    public void testPersonBuilder() {
        final UUID expectedId = UUID.randomUUID();
        final InternalArticle fromBuilder = InternalArticle.builder()
                .id(expectedId)
                .build();
        assertEquals(expectedId, fromBuilder.getId());

    }

    /**
     * Test person constructor.
     */
    @Test
    public void testPersonConstructor() {
        final UUID expectedId = UUID.randomUUID();
        final InternalArticle fromNoArgConstructor = new InternalArticle();
        fromNoArgConstructor.setId(expectedId);
        assertEquals(expectedId, fromNoArgConstructor.getId());
    }
}