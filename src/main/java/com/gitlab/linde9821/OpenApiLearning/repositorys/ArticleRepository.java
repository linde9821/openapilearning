package com.gitlab.linde9821.OpenApiLearning.repositorys;

import com.gitlab.linde9821.OpenApiLearning.mod.InternalArticle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ArticleRepository extends JpaRepository<InternalArticle, UUID> {
}
