package com.gitlab.linde9821.OpenApiLearning.controller;


import com.gitlab.linde9821.OpenAPILearning.generated.api.NewsApiDelegate;
import com.gitlab.linde9821.OpenAPILearning.generated.transfermodel.Article;
import com.gitlab.linde9821.OpenApiLearning.repositorys.ArticleRepository;
import com.gitlab.linde9821.OpenApiLearning.services.TransfermodelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class NewsApiControllerDelegate implements NewsApiDelegate {

    private final ArticleRepository articleRepository;
    private TransfermodelConverter transfermodelConverter;

    @Autowired
    public NewsApiControllerDelegate(ArticleRepository articleRepository,
                                     TransfermodelConverter transfermodelConverter) {
        this.articleRepository = articleRepository;
        this.transfermodelConverter = transfermodelConverter;
    }

    @Override
    public ResponseEntity<List<Article>> getNews() {
        List<Article> articles = articleRepository.findAll().stream()
                .map(article -> transfermodelConverter.MArticleToTArticle(article))
                .collect(Collectors.toList());
        return ResponseEntity.ok(articles);
    }

    @Override
    public ResponseEntity<Article> postNews(Article article) {
        article.setId(UUID.randomUUID());
        return ResponseEntity.ok(transfermodelConverter.MArticleToTArticle(
                articleRepository.save(transfermodelConverter.TArticleToMArticle(article))));
    }
}
