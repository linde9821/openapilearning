package com.gitlab.linde9821.OpenApiLearning.controller;

import com.gitlab.linde9821.OpenAPILearning.generated.api.NewArticleApiDelegate;
import com.gitlab.linde9821.OpenAPILearning.generated.transfermodel.Article;
import com.gitlab.linde9821.OpenApiLearning.mod.InternalArticle;
import com.gitlab.linde9821.OpenApiLearning.repositorys.ArticleRepository;
import com.gitlab.linde9821.OpenApiLearning.services.ArticleService;
import com.gitlab.linde9821.OpenApiLearning.services.TransfermodelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class NewArticleControllerDelegate implements NewArticleApiDelegate {

    private final ArticleRepository articleRepository;
    private final ArticleService articleService;
    private final TransfermodelConverter transfermodelConverter;

    @Autowired
    public NewArticleControllerDelegate(
            ArticleRepository articleRepository,
            ArticleService articleService,
            TransfermodelConverter transfermodelConverter) {
        this.articleRepository = articleRepository;
        this.articleService = articleService;
        this.transfermodelConverter = transfermodelConverter;
    }

    @Override
    public ResponseEntity<Article> addRandomNews() {
        InternalArticle savedArticle = articleRepository.save(articleService.getRandomArticle());
        return ResponseEntity.ok(transfermodelConverter.MArticleToTArticle(savedArticle));
    }
}
