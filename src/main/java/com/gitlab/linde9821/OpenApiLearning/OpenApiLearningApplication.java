package com.gitlab.linde9821.OpenApiLearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenApiLearningApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenApiLearningApplication.class, args);
    }

}
