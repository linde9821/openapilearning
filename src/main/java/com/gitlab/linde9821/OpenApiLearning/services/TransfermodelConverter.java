package com.gitlab.linde9821.OpenApiLearning.services;

import com.gitlab.linde9821.OpenAPILearning.generated.transfermodel.Article;
import com.gitlab.linde9821.OpenApiLearning.mod.InternalArticle;
import org.springframework.stereotype.Service;

@Service
public class TransfermodelConverter {
    public InternalArticle TArticleToMArticle(Article articleToConvert) {
        return InternalArticle.builder()
                .id(articleToConvert.getId())
                .date(articleToConvert.getDate())
                .description(articleToConvert.getDescription())
                .imageUrl(articleToConvert.getImageUrl())
                .title(articleToConvert.getTitle())
                .build();
    }

    public Article MArticleToTArticle(InternalArticle internalArticleToConvert) {
        Article convertedArticle = new Article();

        convertedArticle.setTitle(internalArticleToConvert.getTitle());
        convertedArticle.setDescription(internalArticleToConvert.getDescription());
        convertedArticle.setDate(internalArticleToConvert.getDate());
        convertedArticle.setImageUrl(internalArticleToConvert.getImageUrl());
        convertedArticle.setId(internalArticleToConvert.getId());

        return convertedArticle;
    }
}
