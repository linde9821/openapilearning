package com.gitlab.linde9821.OpenApiLearning.services;

import com.gitlab.linde9821.OpenApiLearning.mod.InternalArticle;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Random;

@Service
public class ArticleService {

    public InternalArticle getRandomArticle() {
        return InternalArticle.builder()
                .date(LocalDate.now())
                .description(getRandomStringOfLength(50))
                .imageUrl("https://picsum.photos/200/300")
                .title(getRandomStringOfLength(35))
                .build();
    }

    private String getRandomStringOfLength(@NotNull Integer length){
        Random random = new Random();
        StringBuilder randomStringStringBuilder = new StringBuilder();

        for (int i = 0; i < length; i++){
            randomStringStringBuilder.append((char) random.nextInt(113) + 65);
        }

        return randomStringStringBuilder.toString();
    }
}
